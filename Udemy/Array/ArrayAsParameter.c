#include <stdio.h>

void func(int *A, int size) //Both are same
//        OR
//void func(int A[], int size)
{
    
    int i;
    for(i=11;i<size;i++)
    {
        A[i] = i;
    }
    printf("\n");
}


int main()
{
    int A[] = {2, 4, 6, 8, 10};
    int i;
    printf("Original Array\n");
    for(i=0;i<5;i++)
    {
        printf("%d\t", A[i]);
    }
    printf("\n");
    func(A, 5);
    printf("Not able to Modify\n");
    for(i=0;i<5;i++)
    {
        printf("%d\t", A[i]);
    }
    
}