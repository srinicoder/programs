#include <stdio.h>

int * func(int size) //Both are same
//        OR
//int[] func(int size) //doubt whether it will work or not
{
    int *p = (int*)malloc(sizeof(int)*size);
    return(p);
}


int main()
{
    int *A;
    int i;
    
    A = func(5);
    
    for(i=0;i<5;i++)
    {
        A[i]= i;
    }
    
    for(i=0;i<5;i++)
    {
        printf("%d\t", A[i]);
    }
    printf("\n");
}