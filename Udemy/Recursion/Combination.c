#include <stdio.h>
int Comb(int n, int r);

int main()
{
    int n = 4, r = 2;
    printf("Output = %d\n", Comb(n, r));
    return 0;
}

int Comb(int n, int r)
{
    if(r == 0 || r == n)
        return 1;
    else
    {
        return Comb(n-1, r-1)+Comb(n-1,r);
    }
}