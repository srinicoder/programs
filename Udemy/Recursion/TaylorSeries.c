#include <stdio.h>

double TaylorSeries(int x, int n);
int main()
{
    int num = 15 ,x = 4;
    
    printf("%f\n", TaylorSeries(x,num));
    return 0;
}

double TaylorSeries(int x, int n)
{
    static double p=1,f=1;
    double r;
    if(n == 0)
    {
        return 1;
    }
    else
    {
        r=TaylorSeries(x, n-1);
        p=p*x;
        f=f*n;
        return (r+p/f);
    }
}