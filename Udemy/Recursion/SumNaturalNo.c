#include <stdio.h>
int SumFirst_N_NaturalNo(int n);

int main()
{
    int num;
    printf("Enter no. of natural number you want sum");
    scanf("%d", &num);
    printf("Sum ==> %d\n", SumFirst_N_NaturalNo(num));
    return 0;
}

int SumFirst_N_NaturalNo(int n)
{
    if(n==0)
        return 0;
    else
        return SumFirst_N_NaturalNo(n-1)+n;
}
