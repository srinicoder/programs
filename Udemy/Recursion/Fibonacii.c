#include <stdio.h>
int Fibo(int n);

int main()
{
    int num = 5;
    printf("Output = %d\n", Fibo(num));
    
    return 0;
}

int Fibo(int n)
{
    if(n == 0)
        return 0;
    if(n == 1)
        return 1;
    else
    {
        return Fibo(n-2)+Fibo(n-1);
    }
}