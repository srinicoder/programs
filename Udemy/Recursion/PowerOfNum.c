#include <stdio.h>
int Power(int m, int n);

int main()
{
    int num, degree;
    printf("Enter number you want ==>\n");
    scanf("%d", &num);
    
    printf("Enter Power of number you want ==>\n");
    scanf("%d", &degree);

    printf("Power of number ==> %d\n", Power(num, degree));
    return 0;
}

int Power(int m, int n)
{
    if(n==0)
        return 1;
    else
        return m * Power(m, n-1);
}
