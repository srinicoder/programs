#include <stdio.h>


void TOH(int n, int From, int Aux, int To);
int main()
{
    int num;
    int A, B, C;
    A = 1;
    B = 2;
    C = 3;
    printf("Enter number of disk\n");
    scanf("%d", &num);
    
    TOH(num, A, B, C);
    return 0;
}

void TOH(int n, int From, int Aux, int To)
{
    if(n > 0)
    {
        TOH(n-1, From, To, Aux);
        printf("Move from %d to %d\n", From,To);
        TOH(n-1, Aux, From, To);
    }
    return;
}