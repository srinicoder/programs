#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, j;
    
    //1st Method
    printf("------------1st Method-----------\n");
    int A[3][4]={{1,2,3,4},
                {5,6,7,8},
                {9,10,11,12}
                };
    
    for(i = 0; i < 3;i++)
    {
        for(j = 0;j < 4;j++)
        {
            printf("%d\t", A[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    
    //2nd Method
    printf("------------2nd Method-----------\n");
    int *B[3];
    
    B[0] = (int*)malloc(sizeof(int)*4);
    B[1] = (int*)malloc(sizeof(int)*4);
    B[2] = (int*)malloc(sizeof(int)*4);
     
    for(i = 0; i < 3;i++)
    {
        for(j = 0;j < 4;j++)
        {
            printf("%d\t", B[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    
    //3rd Method
    printf("------------3rd Method-----------\n");
    int **C;
    
    C = (int**)malloc(sizeof(int*)*3);
    C[0] = (int*)malloc(sizeof(int)*4);
    C[1] = (int*)malloc(sizeof(int)*4);
    C[2] = (int*)malloc(sizeof(int)*4);
    
    //Copy A to C
    printf("Copy to C from A\n");
    for(i = 0; i < 3;i++)
    {
        for(j = 0;j < 4;j++)
        {
            C[i][j] = A[i][j];
        }
        printf("\n");
    }
    printf("\n");
    
    for(i = 0; i < 3;i++)
    {
        for(j = 0;j < 4;j++)
        {
            printf("%d\t", C[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    
    
    return 0;
}
