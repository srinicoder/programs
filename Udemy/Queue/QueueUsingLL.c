#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int data;
    struct Node *Next;
};
struct Node *front=NULL;
struct Node *rear=NULL;


void Display()
{
    struct Node *p;
    p = front;
    while (p!=NULL)
    {
        printf("%d\t", p->data);
        p=p->Next;
    }
    printf("\n");
}

void Enqueue(int x)
{
    struct Node *t = (struct Node*)malloc(sizeof(struct Node));
    if(t == NULL)
    {
        printf("Queue is full\n");
    }
    else
    {
        t->data = x;
        t->Next = NULL;
        
        if(front == NULL)
        {
           front = rear = t;
        }
        else
        {
            rear->Next = t;
            rear = t;
        }
    }
}

int  Dequeue()
{
    int x=-1;
    struct Node *p;
    if(front == NULL)
    {
        printf("Queue is empty\n");
    }
    else
    {
        p = front;
        x = front->data;
        front = front->Next;
        free(p);
    }
    return x;
}

int main()
{
    Display();
    printf("Enter 1 in queue\n");
    Enqueue(1);
    printf("Enter 2 in queue\n");
    Enqueue(2);
    printf("Enter 4 in queue\n");
    Enqueue(4);
    printf("Enter 7 in queue\n");
    Enqueue(7);
    printf("Enter 8 in queue\n");
    Enqueue(8);
    
    Display();
    
    //Delete Queue
    printf("------------Delete Queue----------\n");
    printf("%d Deleted from queue\n", Dequeue());
    printf("%d Deleted from queue\n", Dequeue());
    printf("%d Deleted from queue\n", Dequeue());
    printf("%d Deleted from queue\n", Dequeue());
    printf("%d Deleted from queue\n", Dequeue());
    printf("%d Deleted from queue\n", Dequeue());    
    
    return 0;
}
