#include <stdio.h>
#include <stdlib.h>


struct Queue
{
    int size;
    int front;
    int rear;
    char *QArray;
};

void CreateQueue(struct Queue *q, int size)
{
    printf("Enter the size required\n");
    q->size=size;
    
    q->front=0;
    q->rear=0;
    q->QArray = (int*)malloc(q->size+1);
}


void Enqueue(struct Queue *q, int x)
{
    if((q->rear+1)%q->size==q->front)
    {
        printf("Queue is full\n");
    }
    else
    {
        q->rear = (q->rear+1)%q->size; //0,1,2,3,
        q->QArray[q->rear]=x;
    }
}

int Dequeue(struct Queue *q)
{
    int x=-1;
    if(q->front == q->rear)
    {
        printf("Queue is empty");
    }
    else
    {
        q->front=q->front+1%q->size;
        x=q->QArray[q->front];   
    }
    return x;
}

void Display(struct Queue q)
{
    int i=0;
    i= q.front+1;    
    do
    {
        printf("%d\t",q.QArray[i]);
        i = (i+1)%q.size;
    }while(i!=(q.rear+1)%q.size);
    printf("\n");
}


int main()
{
    struct Queue qu;
    CreateQueue(&qu, 7);
    
    
    printf("------------Enter Queue----------\n");
    //Enter in queue
    printf("2 entered in queue\n");
    Enqueue(&qu, 2);
    printf("4 entered in queue\n");
    Enqueue(&qu, 4);
    printf("6 entered in queue\n");
    Enqueue(&qu, 6);
    printf("8 entered in queue\n");
    Enqueue(&qu, 8);
    printf("9 entered in queue\n");
    Enqueue(&qu, 9);
    printf("7 entered in queue\n");
    Enqueue(&qu, 7);
    
    
    Display(qu);
    
    
     //Delete Queue
    printf("------------Delete Queue----------\n");
    printf("%d Deleted from queue\n", Dequeue(&qu));
    printf("%d Deleted from queue\n", Dequeue(&qu));
    Display(qu);
    
    printf("12 entered in queue\n");
    Enqueue(&qu, 12);
    printf("13 entered in queue\n");
    Enqueue(&qu, 13);
	printf("14 entered in queue\n");
    Enqueue(&qu, 14);
    
    Display(qu);
    
    return 0;
}
