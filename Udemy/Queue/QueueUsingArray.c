#include <stdio.h>
#include <stdlib.h>

struct Queue
{
    int size;
    int front;
    int rear;
    char *QArray;
};


void CreateQueue(struct Queue *q, int size)
{
    printf("Enter the size required\n");
    q->size=size;
    
    q->front=-1;
    q->rear=-1;
    q->QArray = (int*)malloc(q->size+1);
}

void Display(struct Queue q)
{
    int i;
    for(i=q.front+1;i<=q.rear;i++)
    {
        printf("%d\t", q.QArray[i]);
    }
    printf("\n");
}


void Enqueue(struct Queue *q, int x)
{
    if(q->rear > q->size-1)
    {
        printf("Queue is full\n");
    }
    else
    {
        q->rear++;
        q->QArray[q->rear] = x;
    }
    
}

int Dequeue(struct Queue *q)
{
    int x=-1;
    if(q->rear == q->front)
    {
        printf("Queue is empty");
    }
    else
    {
        q->front++;
        x = q->QArray[q->front];
        return x;
    }
    return x;
}

int IsEmpty(struct Queue *q)
{
    if(q->rear == q->front)
    {
        return 1;
    }
    return 0;
}

int IsFull(struct Queue *q)
{
    if(q->rear < q->size-1)
    {
        return 0;
    }
    return 1;
}


int main()
{
    //Create Queue
    struct Queue qu;
    
    
    CreateQueue(&qu, 5);
    Display(qu);
    
    printf("------------Enter Queue----------\n");
    //Enter in queue
    printf("2 entered in queue\n");
    Enqueue(&qu, 2);
    printf("4 entered in queue\n");
    Enqueue(&qu, 4);
    printf("6 entered in queue\n");
    Enqueue(&qu, 6);
    printf("8 entered in queue\n");
    Enqueue(&qu, 8);
    
    Display(qu);
    
    //Delete Queue
    printf("------------Delete Queue----------\n");
    printf("%d Deleted from queue\n", Dequeue(&qu));
    printf("%d Deleted from queue\n", Dequeue(&qu));
    printf("%d Deleted from queue\n", Dequeue(&qu));
    printf("%d Deleted from queue\n", Dequeue(&qu));
    printf("%d Deleted from queue\n", Dequeue(&qu));
    printf("%d Deleted from queue\n", Dequeue(&qu));
    
    
    Display(qu);
    int QEmpty = IsEmpty(&qu);
    QEmpty?printf("Queue is Empty\n"):printf("Queue is not empty\n");

    int QFull = IsFull(&qu);
    QFull?printf("Queue is Full\n"):printf("Queue is not full\n");

    return 0;
}
