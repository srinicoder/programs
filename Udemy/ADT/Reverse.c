#include <stdio.h>
struct Array 
{
    int a[20];
    int size;
    int length;
};

void Reverse(struct Array *arr)
{
    int i,j;
    for(i=0, j=arr->length-1; i<arr->length;i++,j--)
    {
        if(i<=j)
        {
            //Swap without third variable
            arr->a[i] = arr->a[i] + arr->a[j];
            arr->a[j] = arr->a[i] - arr->a[j];
            arr->a[i] = arr->a[i] - arr->a[j];
            
            //Regular swap
            /*
            int temp;
            temp = arr->a[i];
            arr->a[i] = arr->a[j];
            arr->a[j] = temp;
            */
        }
    }
}

int main()
{
    int i;
    struct Array arr = {{8, 3, 5, 1, 9, 2, 4, 11, 6, 7}, 20, 10};
    printf("......Reverse....\n");
    Reverse(&arr);
    for(i=0;i< arr.length;i++)
    {
        printf("%d\t", arr.a[i]);
    }
    printf("\n");

    return 0;
}
