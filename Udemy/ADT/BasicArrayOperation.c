//Basic Array operation
#include <stdio.h>
struct Array
{
    int a[20];
    int size;
    int length;
};

void Display(struct Array arr)
{
    int i;
    for(i=0;i<arr.length;i++)
    {
        printf("%d\t", arr.a[i]);
    }
    printf("\n");
}

void Insert(struct Array *arr, int num, int position)
{
    int i;
    for(i=arr->length;i>position;i--)
    {
        arr->a[i] = arr->a[i-1];
    }
    arr->a[position] = num;
    arr->length++;
}

void Delete(struct Array *arr, int position)
{
    int i;
    for(i = position;i<arr->length-1;i++)
    {
        arr->a[i] = arr->a[i+1];
    }
    arr->length--;
    
}

void Append(struct Array *arr, int num)
{
    arr->a[arr->length] = num;
    arr->length++;
}

int LinearSearch(struct Array arr, int key)
{
    int i;
    for(i=0;i<arr.length;i++)
    {
        if(arr.a[i] == key)
        {
            return i;
        }
    }
    return -1;
}

int main()
{
    struct Array arr={{1,2,3,4,5,6,7}, 20, 7};
    printf(".....Original Array.....\n");
    Display(arr);
    
    Append(&arr, 11);
    printf(".......Append.......\n");
    Display(arr);
    
    Insert(&arr, 9, 4);
    printf("......Insertion .....\n");
    Display(arr);
    
    Delete(&arr,4);
    printf("....Deletion......\n");
    Display(arr);
    
    int index = LinearSearch(arr, 4);
    if(index ==-1)
    {
        printf("Key not found in list\n");
    }
    else
    {
        printf("Key found in index ===> %d\n", index);
    }
    return 0;
}
