include <stdio.h>

struct Array 
{
    int a[12+1];
    int size;
    int len;
};

void RotateLeftby2(struct Array *arr, int len, int rotateby)
{
    int i,j;
    int temp;
    for(i = 0; i< rotateby;i++)
    {
        temp = arr->a[0];
        for(j = 0;j<len-1;j++)
        {
            arr->a[j] = arr->a[j+1];
        }
        arr->a[j] = temp;
        printf("\n");
    }
    
}

void Display(struct Array arr, int size)
{
    int i;
    for(i = 0;i< size;i++)
    {
        printf("%d\t", arr.a[i]);
    }
    printf("\n");
}

int main()
{
    struct Array arr = {{3, 4, 6, 7, 1, 2, 8}, 13, 7};
    RotateLeftby2(&arr, arr.len, 2);
    Display(arr, arr.len);
    return 0;
}
