#include <stdio.h>
struct Array 
{
    int a[20];
    int size;
    int length;
};

int Sum(struct Array arr)
{
    int i, total=0;
    for (i=0;i<arr.length;i++)
    {
        total = total+arr.a[i]; 
    }
    return total;
}


int R_Sum(struct Array arr, int num)
{
    if(num < 0)
    {
        return 0;
    }
    return R_Sum(arr,num-1) + arr.a[num];
}

int main()
{
    struct Array arr = {{8, 3, 5, 1, 9, 2, 4, 11, 6, 7}, 20, 10};
    
    printf("SUM --> %d\n", Sum(arr));
    
    printf("Recursive SUM --> %d\n", R_Sum(arr, arr.length));

    return 0;
}
