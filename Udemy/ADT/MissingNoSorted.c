#include <stdio.h>
struct Array 
{
    int a[40];
    int size;
    int length;
};

int MissingNoSorted(struct Array arr)
{
    int i, diff;
    diff = arr.a[0]-0;
    for(i=1;i<arr.length;i++)
    {
        if(arr.a[i]-i != diff)
        {
            while(diff < arr.a[i]-i)
            {
                printf("%d\t", (i+diff));
                diff++;
            }
        }
    }
}


int main()
{
    struct Array arr = {{1,2,3,4,5,6,7,8,23,11}, 20, 10};
    MissingNoSorted(arr);
    return 0;
}


