#include <stdio.h>
struct Array 
{
    int a[21];
    int size;
    int length;
};

void Merge(struct Array arr1, struct Array arr2, struct Array *arr3)
{
    int i,j,k;
    for(k=0,i=0,j=0;i < arr1.length && j < arr2.length;k++)
    {
        if(arr1.a[i] < arr2.a[j])
        {
            arr3->a[k] = arr1.a[i];
            i++;
        }
        else
        {
            arr3->a[k] = arr2.a[j];
            j++;
        }
    }  
    
    for(;i< arr1.length;i++)
    {
        arr3->a[i] = arr1.a[i];
    }
    
    for(;j< arr2.length;j++)
    {
        arr3->a[k] =  arr2.a[j];
    }
}


int main()
{
    int i;
    //Both array need to be sorted
    struct Array arr1 = {{3, 12, 13, 34, 45, 56, 71, 88, 99, 100}, 21, 10};
    struct Array arr2 = {{1, 22, 33, 44, 58, 69, 77, 89, 91, 103}, 21, 10};
    struct Array arr3 ={{0}, 21, 10};
    Merge(arr1, arr2, &arr3);
    
    for(i = 0;i< 21;i++)
    {
        printf("%d\t", arr3.a[i]);
    }
    printf("\n");
    return 0;
}
