
#include <stdio.h>

struct Array
{
    int a[20];
    int size;
    int length;
};

int R_BinarySearch(struct Array arr, int low, int high, int key)
{
    int mid;
    if(low<=high)
    {
        mid =(low+high)/2;
        
        if(arr.a[mid] == key)
        {
            return mid;
        }
        
        else if(arr.a[mid] < key)
        {
            return R_BinarySearch(arr, mid+1, high, key);
        }
        
        else if(arr.a[mid] > key)
        {
            return R_BinarySearch(arr, low,mid-1, key);
        }
    } 
    return -1;
}

int main()
{
    struct Array arr={{4,8,10,15,18,21,24,27,29,33,34,37,39,41,43}, 20, 15};
    
    int index = R_BinarySearch(arr, 0, arr.length-1, 34);
    if(index == -1)
    {
        printf("Not found in list\n");
    }
    else
    {
        printf("Search item is in index %d\n", index);
    }
    return 0;
}
