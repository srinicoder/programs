#include <stdio.h>

struct Array
{
    int a[20];
    int size;
    int length;
};

int BinarySearch(struct Array arr, int low, int high, int key)
{
    int mid;

    while(low<=high)
    {
        mid = (low+high)/2;
        if(arr.a[mid] == key)
        {
            return mid;
        }
        
        if(arr.a[mid] < key)
        {
            low = mid+1;
        }
        
        else if(arr.a[mid] > key)
        {
            high = mid-1;
        }
    } 
    return -1;
}

int main()
{
    struct Array arr={{4,8,10,15,18,21,24,27,29,33,34,37,39,41,43}, 20, 15};
    
    int index = BinarySearch(arr, 0, arr.length-1, 4);
    if(index == -1)
    {
        printf("Not found in list\n");
    }
    else
    {
        printf("Search item is in index %d\n", index);
    }
    return 0;
}
