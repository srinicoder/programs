#include <stdio.h>
struct Array 
{
    int a[20];
    int size;
    int length;
};

int Max(struct Array arr)
{
    int i, max;
    max = arr.a[0];
    for(i=0;i<arr.length;i++)
    {
        if(max < arr.a[i])
        {
            max = arr.a[i];
        }
    }
    return max;
}

int Min(struct Array arr)
{
    int i, min;
    min = arr.a[0];
    for(i=0;i<arr.length;i++)
    {
        if(arr.a[i] < min)
        {
            min = arr.a[i];
        }
    }
    return min;
}


int main()
{
    struct Array arr = {{8, 3, 5, 1, 9, 2, 4, 11, 6, 7}, 20, 10};
    printf("Max item --> %d\n", Max(arr));
    printf("Min item --> %d\n", Min(arr));

    return 0;
}
