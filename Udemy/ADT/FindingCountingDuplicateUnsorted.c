#include <stdio.h>
struct Array 
{
    int a[40];
    int size;
    int length;
};

void DuplicateUnsorted(struct Array arr)
{
    int i,j, count;
    for(i=0;i<arr.length;i++)
    {
        count =1;
        if(arr.a[i] != -1)
        {
            for(j=i+1;j<arr.length-1;j++)
            {
                if(arr.a[i] == arr.a[j])
                {
                    count++;
                    arr.a[j] = -1;            
                }
            }
            if(count>1)
            {
                    printf("%d repeats %d times\n", arr.a[i], count);
            }
       }
    }   
}

int main()
{
    struct Array arr = {{3, 6, 8, 8, 10,12, 15,15,15,20}, 20, 10};
    DuplicateUnsorted(arr);
    return 0;
}
