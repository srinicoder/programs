#include <stdio.h>
struct Array 
{
    int a[20];
    int size;
    int length;
};

int ChkIfSorted(struct Array arr)
{
    int i;
    for(i = 0; i < arr.length-1;i++)
    {
        if(arr.a[i+1]<arr.a[i])
        {
            return 1;
        }
    }
    return 0;
}
int main()
{
    int i;
    //struct Array arr = {{8, 3, 5, 1, 9, 2, 4, 11, 6, 7}, 20, 10};
    struct Array arr = {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 20, 10};
    printf("......ChkIfSorted....\n");
    int iSorted = ChkIfSorted(arr);
    if(iSorted == 0)
    {
        printf("Array is Sorted\n");
    }
    else
    {
        printf("Array is not sorted\n");
    }
    
    printf("\n");

    return 0;
}
