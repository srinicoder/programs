#include <stdio.h>
#include <stdlib.h>

struct stack
{
    int data;
    struct stack *Next;
};

struct stack *top = NULL;

void Display()
{
    struct stack *p;
    p = top;
    while(p!=NULL)
    {
        printf("%d\t", p->data);
        p = p->Next;
    }
    printf("\n");
}


void Push(int element)
{
    struct stack *t;
    t = (struct stack*)malloc(sizeof(struct stack));
    if(t==NULL)
    {
        printf("Stack Overflow\n");   
    }
    else
    {
        t->data = element;
        t->Next = top;
        top = t;
    }
}

int Pop()
{
    struct stack *t;
    int element;
    if(top == NULL)
    {
        printf("Stack is Empty\n");
    }
    else
    {
        t = top;
        top = top->Next;
        element = t->data;
        free(t);
        return element;
    }
}

int Peek(int pos)
{
    struct stack *p;
    int i, element =-1;
    p = top;
    for(i =0;i<pos-1 && p!=NULL;i++)
    {
        p=p->Next;
    }
    if(p!=NULL)
    {
        element = p->data;
        return element;
    }
    else
    {
        printf("Invalid Position \n");
        return -1;
    }
}


int IsFull()
{
    int iret;
    struct stack *t = (struct stack*)malloc(sizeof(struct stack));
    iret = (t!=NULL) ? 1 : 0;
    free(t);
    return iret;
}

int IsEmpty()
{
    int iret;
    iret = (top!=NULL) ? 0 : 1;
    return iret;
}

int main()
{
    
    //Push
    printf("-------Push------\n");
    Push(3);
    Push(5);
    Push(6);
    Push(7);
    Push(11);
    Push(13);
    printf("\nDisplay after Push\n");
    Display();
    
    
    printf("-------Pop------\n");
    printf("%d\t", Pop());
    printf("%d\t", Pop());
    printf("%d\t", Pop());
    
    printf("\nDisplay after POP\n");
    Display();
    
    printf("---------Peek--------\n");
    printf("Peek at positon 1 is %d\n", Peek(1));
    printf("Peek at positon 2 is %d\n", Peek(2));

    printf("\nDisplay after Peek\n");
    Display();
    printf("Is Stack is Full?\n");
    int ret = IsFull();
    if(ret == 1)
    {
        printf("Stack is Full\n");
    }
    else
    {
        printf("Stack is not Full\n");
    }
    
    printf("Is Stack is Empty?\n");
    ret = IsEmpty();
    if(ret == 1)
    {
        printf("Stack is Empty\n");
    }
    else
    {
        printf("Stack is not Empty\n");
    }
    
    return 0;
}
