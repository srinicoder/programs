#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct stack
{
    char data;
    struct stack *Next;
};

struct stack *top = NULL;

void Display()
{
    struct stack *p;
    p = top;
    while(p!=NULL)
    {
        printf("%c\t", p->data);
        p = p->Next;
    }
    printf("\n");
}


void Push(char element)
{
    struct stack *t;
    t = (struct stack*)malloc(sizeof(struct stack));
    if(t==NULL)
    {
        printf("Stack Overflow\n");   
    }
    else
    {
        t->data = element;
        t->Next = top;
        top = t;
    }
}

int Pop()
{
    struct stack *t;
    char element;
    if(top == NULL)
    {
        printf("Stack is Empty\n");
    }
    else
    {
        t = top;
        top = top->Next;
        element = t->data;
        free(t);
        return element;
    }
}

int Peek(int pos)
{
    struct stack *p;
    int i; 
    char element =-1;
    p = top;
    for(i =0;i<pos-1 && p!=NULL;i++)
    {
        p=p->Next;
    }
    if(p!=NULL)
    {
        element = p->data;
        return element;
    }
    else
    {
        printf("Invalid Position \n");
        return -1;
    }
}


int IsFull()
{
    int iret;
    struct stack *t = (struct stack*)malloc(sizeof(struct stack));
    iret = (t!=NULL) ? 1 : 0;
    free(t);
    return iret;
}

int IsEmpty()
{
    int iret;
    iret = (top!=NULL) ? 0 : 1;
    return iret;
}

int StackTop()
{
    if(top!=NULL)
    {
        return top->data;
    }
}

int IsOperand(char x)
{
    if(x == '+' || x == '-' || x == '*' || x == '/')
    {
        return 1;
    }
    else
    {
        return 0;
    }
    
}

int PrecedenceLevel(char x)
{
    if(x == '+' || x == '-')
    {
        return 1;
    }
    else if(x == '*' || x == '/')
    {
        return 2;
    }
    return 0;
}

char *ConvertInfixToPostfix(char *Infix)
{
    int i = 0, j = 0;
    char *postfix = (char*)malloc(strlen(Infix)+1);
    
    while(Infix[i]!='\0')
    {
        if(IsOperand(Infix[i]) == 0) //If operand copy into postfix
        {
            postfix[j++] = Infix[i++];
        }
        else //If operator insert into stack
        {
            if(PrecedenceLevel(Infix[i]) > PrecedenceLevel(StackTop()/*top->data*/))//Input precedence > top stack precedence Push
            {
                Push(Infix[i++]);
            }
            else //Pop out top and copy into postfix
            {
                postfix[j++] = Pop();
            }
        }
    }
    while(!IsEmpty()) //copy into postfix all remaining element
    {
        postfix[j++]= Pop();
        postfix[j] = '\0';
    }
    return postfix;
}

int main()
{
    int i;
    char *Infix = "a+b*c-d/e";
    char *OutputPostfix;
    OutputPostfix = ConvertInfixToPostfix(Infix);
    printf("%s\n", OutputPostfix);
    free(OutputPostfix);
    return 0;
}