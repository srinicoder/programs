#include <stdio.h>
#include <stdlib.h>

struct stack
{
    int size;
    int *S;
    int top;
};


void Create(struct stack *st)
{
    //Get the size
    printf("Enter the size of the stack\n");
    scanf("%d", &st->size);
  
    st->S = (int*)malloc(sizeof(int)*st->size);  
    st->top = -1;
}

void Push(struct stack *st, int element)
{
    if(st->top == st->size-1)
    {
        printf("Stack Overflow\n");
        
    }
    else
    {
        st->top++;
        st->S[st->top] = element;
    }
    return;
}

int Pop(struct stack *st)
{
    int element =-1;
    if(st->top == -1)
    {
        printf("\nStack Underflow\n");
    }
    else
    {
        element = st->S[st->top];
        st->top--;
    }
    return element;
}

int Peek(struct stack st, int pos)
{
    int element=-1;
    if(st.top-pos+1 < 0)
    {
        printf("\nInvalid Position\n");
    }
    else
    {
        element = st.S[st.top-pos+1];
    }
    return element;
}


void Display(struct stack st)
{
    int i, index = 0;
    for(i=st.top;i>=0;i--)
    {
        printf("%d\t", st.S[index++]);
    }
    printf("\n");
}

int IsFull(struct stack st)
{
    if(st.top == st.size-1)
    {
        return 1;
    }
    return 0;
}

int IsEmpty(struct stack st)
{
    if(st.top==-1)
    {
        return 1;
    }
    return 0;
}

int main()
{
    struct stack St;
    //Create
    Create(&St);
    
    //Push
    printf("-------Push------\n");
    Push(&St, 3);
    Push(&St, 5);
    Push(&St, 6);
    Push(&St, 7);
    Push(&St, 11);
    Push(&St, 13);
    printf("Top ... %d\n", St.top);
     printf("\nDisplay after Push\n");
    Display(St);
    
    printf("-------Pop------\n");
    printf("%d\t", Pop(&St));
    printf("%d\t", Pop(&St));
    printf("%d\t", Pop(&St));
    
    printf("\nDisplay after POP\n");
    Display(St);

    printf("---------Peek--------\n");
    printf("Peek at positon 1 is %d\n", Peek(St, 1));
    printf("Peek at positon 5 is %d\n", Peek(St, 5));

    printf("\nDisplay after Peek\n");
    Display(St);

    printf("Is Stack is Full?\n");
    int ret = IsFull(St);
    if(ret == 1)
    {
        printf("Stack is Full\n");
    }
    else
    {
        printf("Stack is not Full\n");
    }
    
    printf("Is Stack is Empty?\n");
    ret = IsEmpty(St);
    if(ret == 1)
    {
        printf("Stack is Empty\n");
    }
    else
    {
        printf("Stack is not Empty\n");
    }
    return 0;
}
