#include <stdio.h>

struct Rectangle
{
    int length;
    int breadth;
};

int main()
{
    struct Rectangle r ={10,5};
    printf("Original len = %d\n", r.length);
    struct Rectangle *p = &r;
    (*p).length = 5;
    printf("Modified len = %d\n", (*p).length);
    
    p->length = 3;
    printf("Again Modified len = %d\n", p->length);
    return 0;
}
