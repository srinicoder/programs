#include <stdio.h>

struct Rectangle
{
    int length;
    int breadth;
};

int  Area(struct Rectangle r1)
{
    return r1.length*r1.breadth;

}

int main()
{
    struct Rectangle r ={10,5};
    printf("Area --> %d\n", Area(r));

    return 0;
}
