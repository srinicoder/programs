#include <stdio.h>

struct Rectangle
{
    int length;
    int breadth;
};

void ChangeLength(struct Rectangle *r1, int l)
{
    r1->length = l;
    return;
}

int main()
{
    struct Rectangle r ={10,5};
    ChangeLength(&r, 15);
    printf("changed length --> %d\n", r.length);

    return 0;
}
