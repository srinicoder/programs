include <stdio.h>

struct Rectangle
{
    int length;
    int breadth;
};

int main()
{
    struct Rectangle *p = (struct Rectangle*)malloc(sizeof(struct Rectangle));
    p->length  = 5;
    p->breadth = 6;
    
    printf("length:%d\tbreadth:%d\n", p->length, p->breadth);
    return 0;
}
