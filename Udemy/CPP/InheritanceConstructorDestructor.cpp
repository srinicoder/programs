#include <iostream>

using namespace std;

class Base
{
    int num;
    public:
    Base()
    {
        cout << "Base no args constructor" << endl;
    }
    
    Base(int num_val):num{num_val}
    {
        cout << "Base Single arg constructor" << endl;
    }
    
    ~Base()
    {
        cout << "Base Destructor" << endl;    
    }
    
};


class Derived:public Base
{
   // using Base::Base; //This is important.
    double num_double;
    public:
    Derived()
    {
        cout << "Derived no args constructor" << endl;
    }
    
    
    Derived(double num_double_val):num_double{num_double_val*2}
    {
        cout << "Derived Single arg constructor" << endl;
    }
    
    ~Derived()
    {
        cout << "Derived Destructor" << endl;    
    }
    
};


int main()
{
   // Base b;
    //Base b(100);
    
    //Derived d;
    Derived d(200);
    return 0;
}
