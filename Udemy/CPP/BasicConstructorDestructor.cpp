#include <iostream>

using namespace std;

class Players
{
    private:
    string name;
    int health;
    int xp;
    
    public:
    void SetName(string name_val)
    {
        name = name_val;
    }
    
    Players()
    {
        cout << "Constructor with no argument" << endl;
    }
    Players(string name_val)
    {
        cout << "Constructor with one argument" << endl;
    }
    Players(string name_val, int health_val, int xp_val)
    {
        cout << "Constructor with three argument" << endl;
    }
    
    ~Players()
    {
        cout << "Destructor called with " << name << endl;
    }
    
};

int main()
{
    {
        Players splayers;
        splayers.SetName("Srinivasan A");
        
    }
    
    {
        Players Hero("Hero");
        Hero.SetName("Hero");
        
        Players Villian("Villian", 100, 10);
        Villian.SetName("Villian");
    }
    
        Players *Enemy = new Players;
        Enemy->SetName("Enemy");
        
        Players *LevelBoss = new Players;
        LevelBoss->SetName("LevelBoss");
        
        delete Enemy;
        delete  LevelBoss;

    return 0;
}