#include <iostream>

using namespace std;

class Base
{
    int value;
    public:
    //No arg constructor
    Base():value{0}
    {
        cout << "Base No arg Constructor" << endl;
    }
    
    //Overloading 1 arg constructor
    Base(int new_value):value{new_value}
    {
        cout << "Overloading (int) Constructor" << endl;
    }
    
    //Base copy constructor
    Base(const Base &other):value{other.value}
    {
        cout << "Base copy constructor" << endl;    
    }
    
    //= operator overloading
    Base &operator=(const Base &rhs)
    {
        cout <<"Base operator= overloading" <<endl;
        if(this == &rhs)
        {
            return *this;
        }
        value = rhs.value;
        return *this;
    }
    
    //Base Destructor
    ~Base()
    {
        cout << " Base Destructor" << endl;
    }
};

class Derived:public Base
{
    int double_value;
    public:
    //Derived No arg constructor
    Derived():Base()
    {
        cout << "Derived No arg Constructor" << endl;
    }
    
    // Derived Overloading 1 arg constructor
    Derived(int new_double_value):Base(new_double_value), double_value{new_double_value}
    {
        cout << "Derived Overloading (int) Constructor" << endl;
    }
    
    //Derived copy constructor
    Derived(const Derived &other):Base(other), double_value{other.double_value}
    {
        cout << "Derived copy constructor" << endl;    
    }
    
    //= operator overloading
    Derived &operator=(const Derived &rhs)
    {
        cout <<"Derived operator= overloading" <<endl;
        if(this == &rhs)
        {
            return *this;
        }
        Base::operator=(rhs);  //Important step
        double_value = rhs.double_value;
        return *this;
    }
    
    //Derived Destructor
    ~Derived()
    {
        cout << " Derived Destructor" << endl;
    }
};


int main()
{
    /*
    Base b;
    Base b1(b);
    b=b1;
    */
    
    Derived d;
    Derived d1(d);
    d=d1;
    
    return 0;
}