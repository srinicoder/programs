
#include <iostream>
#include "Account.h"
#include "SavingAccount.h"

using namespace std;

int main()
{
    Account acc;
    acc.Deposit(2000.0);
    acc.Withdrawal(500.0);

    Account *p_acc(nullptr);
    p_acc = new Account();
    p_acc->Deposit(1000.0);
    p_acc->Withdrawal(500.0);

    SavingAccount savacc;
    savacc.Deposit(2000.0);
    savacc.Withdrawal(500.0);

    SavingAccount *p_savacc(nullptr);
    p_savacc = new SavingAccount();
    p_savacc->Deposit(1000.0);
    p_savacc->Withdrawal(500.0);

    return 0;
}
