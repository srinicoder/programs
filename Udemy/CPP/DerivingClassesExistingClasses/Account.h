#ifndef _ACCOUNT_H_
#define _ACCOUNT_H_

class Account
{
    public:
    double balance;
    std::string name; 
    Account():balance{0.0}, name{"Is an Account"}
    {
        
    }
    
    void Deposit(double Amt);
    void Withdrawal(double Amt);
    
    ~Account()
    {
        
    }
};
#endif