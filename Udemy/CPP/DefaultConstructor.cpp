include <iostream>

using namespace std;

class Players
{
    private:
    string name;
    int health;
    int xp;
    
    public:
    void SetName(string name_val)
    {
        name = name_val;
    }
    
    Players()
    {
        name = "None";
        health = 0;
        xp = 0;
        cout << "Explicit default constructor" << endl;
    }
    
    Players(string name_val,int health_val)
    {
        name = name_val;
        health = health_val;
        xp = 0;
        cout << "Parameterized constructor" << endl;
    }
    
    
};

int main()
{

    Players splayers;
    splayers.SetName("Srinivasan A");
        

    Players Hero("Srinivasan A", 30);
    Hero.SetName("Srinivasan A");
    
    return 0;
}