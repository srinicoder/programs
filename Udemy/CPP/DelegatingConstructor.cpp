#include <iostream>

using namespace std;

class Players
{
  string name;
  int health;
  int xp;
  
  public:
  
  string GetName(){return name;}
  int GetHealth(){return health;}
  int GetXp(){return xp;}
  
  Players(string name_val, int health_val, int xp_val);
  Players(string name_val, int health_val);
  Players(string name_val);
  Players();
  
  ~Players()
  {
      
  }
};

  Players::Players(string name_val, int health_val, int xp_val):name{name_val},health{health_val}, xp{xp_val}
  {
      cout << "Constructor with three args" << endl;
  }
  
 
  Players::Players(string name_val, int health_val):Players(name_val,health_val, 0)
  {
      cout << "Delegating Constructor with two args" << endl;
  }
  
  Players::Players(string name_val):Players(name_val, 0, 0)
  {
      cout << "Delegating Constructor with one args" << endl;
  }
  
  Players::Players():Players("None", 0, 0)
  {
      cout << "Delegating Constructor with no args" << endl;
  }

void Display(Players P)
{
    cout << "Name: " << P.GetName() << endl;
    cout << "Health: " << P.GetHealth() << endl;
    cout << "Xp: " << P.GetXp() << endl;
}

int main()
{
    Players Srini("Srini", 100, 10);
    Display(Srini);
    
    Players Hero("Hero", 50);
    Display(Hero);
    
    Players Villian("Villian");
    Display(Villian);
    
    Players Enemy;
    Display(Enemy);

    return 0;
}
