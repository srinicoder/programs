#include <iostream>
#include "Account.h"

using namespace std;

//No Arg Constructor
Account::Account():Balance{0.0}
{
            
}

//Single Arg Constructor
Account::Account(double balance):Balance{balance}
{
            
}

//Deposit
void Account::Deposit(double Amt)
{
    Balance = Balance+Amt;
}

//Withdrawal
void Account::Withdrawal(double Amt)
{
    if(Balance-Amt >= 0)
    {
        Balance = Balance-Amt;
    }
    else
    {
        cout << "Insufficient fund" << endl;
    }
    
}

//Destructor
Account::~Account()
{
    
}

//Global operator overloading
ostream & operator<<(ostream &os, const Account &account)
{
    os << "Account Balance is: " << account.Balance;
    return os;
}
