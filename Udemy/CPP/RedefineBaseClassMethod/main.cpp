#include <iostream>
#include "Account.h"
#include "SavingAccount.h"

using namespace std;

int main()
{
    cout << "========Account class=========" << endl;
    Account a1{1000};
    cout << a1 << endl;
    
    a1.Deposit(500);
    cout << a1 << endl;

    a1.Withdrawal(50);
    cout << a1 << endl;
    
    a1.Withdrawal(2000);
    cout << a1 << endl;
    
    cout << "==========Saving Account class=======" << endl;
    
    SavingAccount sa1{1000, 5};
    cout << sa1 << endl;
    
    sa1.Deposit(500);
    cout << sa1 << endl;
    
    sa1.Withdrawal(50);
    cout << sa1 << endl;
    
    sa1.Withdrawal(4000);
    cout << sa1 << endl;
    
    
    return 0;
}