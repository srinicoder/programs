ifndef _ACCOUNT_H_
#define _ACCOUNT_H_

#include <iostream>

class Account
{
    //Global overloading << operator
    friend std::ostream & operator<<(std::ostream &os, const Account &account);
    
    protected:
    double Balance;
    
    public:
    //No Arg constructor
    Account();
    
    //One Arg constructor
    Account(double balance);
    
    //Deposit
    void Deposit(double Amt);
    
    //Withdrawal
    void Withdrawal(double Amt);
    
    //Destructor
    ~Account();
    
};
#endif