#ifndef _SAVINGACCOUNT_H_
#define _SAVINGACCOUNT_H_

#include <iostream>
#include "Account.h"

class SavingAccount:public Account
{
    //Global overloading << operator
    friend std::ostream & operator<<(std::ostream &os, const SavingAccount &savingaccount);
    
    protected:
    double IntRate;
    
    public:
    //No Arg constructor
    SavingAccount();
    
    //One Arg constructor
    SavingAccount(double balance, double intrate);
    
    //Deposit
    void Deposit(double Amt);
    
    //Not required as it is inherited
    /*
    //Withdrawal
    void Withdrawal(double Amt);
    */
    
    //Destructor
    ~SavingAccount();
    
};
#endif