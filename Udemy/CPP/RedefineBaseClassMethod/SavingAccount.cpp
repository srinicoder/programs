#include <iostream>
#include "SavingAccount.h"

using namespace std;

//No Arg Constructor
SavingAccount::SavingAccount():SavingAccount{0.0, 0.0}
{
            
}


//Single Arg Constructor
SavingAccount::SavingAccount(double balance, double intrate):Account(balance), IntRate{intrate}
{
            
}


//Deposit
void SavingAccount::Deposit(double Amt)
{
    Amt = Amt+(Amt*IntRate/100);
    Account::Deposit(Amt);
}


//Since Inherited not required to write again
/*
//Withdrawal
void SavingAccount::Withdrawal(double Amt)
{
    if(Balance-Amt >= 0)
    {
        Balance = Balance-Amt;
    }
    else
    {
        cout << "Insufficient fund" << endl;
    }
    
}
*/
//Destructor
SavingAccount::~SavingAccount()
{
    
}

//Global operator overloading
ostream & operator<<(ostream &os, const SavingAccount &savingaccount)
{
    os << "Saving Account Balance is: " << savingaccount.Balance << " With InterestRate: " << savingaccount.IntRate;
    return os;
}


