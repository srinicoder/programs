#include <iostream>

using namespace std;

class Players
{
    private:
    string name;
    int health;
    int xp;
    
    public:
    Players(string name = "None", int health = 0, int xp = 0);
    void SetName(string name_val)
    {
        name = name_val;
    }
    
    string GetName(void)
    {
        return name;
    }
    
    int GetHealth(void)
    {
        return health;
    }
    
    int GetXp()
    {
        return xp;
    }
    
    Players(const Players &CopyObj);
    
    ~Players()
    {
        cout << "Destructor called for " << name << endl;
    }
    
    
};

Players::Players(const Players &CopyObj):name{CopyObj.name}, health{CopyObj.health}, xp{CopyObj.xp}
{
    cout << "Copy constructor called for " << CopyObj.name << endl;
}

void Display(Players P)
{
    cout << "Name" << P.GetName() << endl;
    cout << "Health" << P.GetHealth() << endl;
    cout << "Xp" << P.GetXp() << endl;
}

Players::Players(string name_val,int health_val, int xp_val):name{name_val}, health{health_val}, xp{xp_val}
{
    cout << "Three arg Parameterized constructor for " << name << endl;
}

int main()
{
    Players empty("XXXXXX", 100, 30);
    Players New_Obj(empty);
    Display(empty);
        
    Players Hero("Hero", 30);
    Hero.SetName("Hero");
    
    Players Villian("Villian", 20, 5);
    Villian.SetName("Villian");
    
    return 0;
}
