#include <iostream>

using namespace std;

class Account
{
    public:
    virtual void Withdraw()
    {
        cout <<"In Account::Withdraw" << endl;
    }
    
};

class Saving:public Account
{
    public:
    virtual void Withdraw()
    {
        cout <<"In Saving::Withdraw" << endl;
    }
    
};

class Checking:public Account
{
    public:
    virtual void Withdraw()
    {
        cout <<"In Checking::Withdraw" << endl;
    }
    
};

class Trust:public Account
{
    public:
    virtual void Withdraw()
    {
        cout <<"In Trust::Withdraw" << endl;
    }
};

int main()
{
    cout << "<=====Pointers=====>" << endl;
    Account     *ptr1 = new Account();
    Account     *ptr2 = new Saving();
    Account     *ptr3 = new Checking();
    Account     *ptr4 = new Trust();
    
    ptr1->Withdraw();
    ptr2->Withdraw();
    ptr3->Withdraw();
    ptr4->Withdraw();
    
    return 0;
}