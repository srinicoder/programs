#include <iostream>

using namespace std;

class Account
{
    public:
    virtual void Withdraw(double amount)
    {
        cout <<"In Account::Withdraw" << endl;
    }
    
};

class Saving:public Account
{
    public:
    virtual void Withdraw(double amount)
    {
        cout <<"In Saving::Withdraw" << endl;
    }
    
};

class Checking:public Account
{
    public:
    virtual void Withdraw(double amount)
    {
        cout <<"In Checking::Withdraw" << endl;
    }
    
};

class Trust:public Account
{
    public:
    virtual void Withdraw(double amount)
    {
        cout <<"In Trust::Withdraw" << endl;
    }
};

void DoWithdraw(Account &account, double amount)
{
    account.Withdraw(amount);
}

int main()
{
    Account a;
    Account &ref = a;
    ref.Withdraw(1000);
    
    Trust t;
    Account &ref1 = t;
    ref1.Withdraw(2000);
    
    Account     a1;
    Saving      a2;
    Checking    a3;
    Trust       a4;
    
    DoWithdraw(a1, 1000);
    DoWithdraw(a2, 2000);
    DoWithdraw(a3, 3000);
    DoWithdraw(a4, 4000);
    
    return 0;
}