#include <iostream>

using namespace std;

class Account
{
    public:
    virtual void Withdraw()
    {
        cout <<"In Account::Withdraw" << endl;
    }
    
    virtual ~Account()
    {
        cout << "Account::Destructor" << endl;
    }
    
};

class Saving:public Account
{
    public:
    virtual void Withdraw()
    {
        cout <<"In Saving::Withdraw" << endl;
    }
    
    virtual ~Saving()
    {
        cout << "Saving::Destructor" << endl;
    }
    
};

class Checking:public Account
{
    public:
    virtual void Withdraw()
    {
        cout <<"In Checking::Withdraw" << endl;
    }
    
    virtual ~Checking()
    {
        cout << "Checking::Destructor" << endl;
    }
    
};

class Trust:public Account
{
    public:
    virtual void Withdraw()
    {
        cout <<"In Trust::Withdraw" << endl;
    }
    
    virtual ~Trust()
    {
        cout << "Trust::Destructor" << endl;
    }
};

int main()
{
    cout << "<=====Pointers=====>" << endl;
    Account     *ptr1 = new Account();
    Account     *ptr2 = new Saving();
    Account     *ptr3 = new Checking();
    Account     *ptr4 = new Trust();
    
    ptr1->Withdraw();
    ptr2->Withdraw();
    ptr3->Withdraw();
    ptr4->Withdraw();
    
    
    cout << "<=====Clean up=====>" << endl;
    
    delete ptr1;
    delete ptr2;
    delete ptr3;
    delete ptr4;
    
    return 0;
}