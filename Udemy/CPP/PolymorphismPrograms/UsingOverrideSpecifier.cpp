#include <iostream>

using namespace std;

class Base
{
    public:
    virtual void SayHello() const
    {
        cout <<"Hello from Base" << endl;
    }
    
    virtual ~Base()
    {
        cout << "Account::Destructor" << endl;
        
    }
    
};

class Derived:public Base
{
    public:
    virtual void SayHello() const override
    {
        cout <<"Hello from Derived" << endl;
    }
    
    virtual ~Derived()
    {
        cout << "Derived::Destructor" << endl;
    }
    
};


int main()
{
 
    Base *bptr1 = new Base();
    bptr1->SayHello();
    
    Derived *dp = new Derived();
    dp->SayHello();
    
    Base *bptr2 = new Derived();
    bptr2->SayHello();
    
    return 0;
}