#include <iostream>

using namespace std;

class Shape
{
    public:
    virtual void Draw() = 0;
    virtual void Rotate() = 0;
    virtual ~Shape(){}
};

class OpenShape:public Shape
{
    public:
    virtual ~OpenShape(){}
};

class ClosedShape:public Shape
{
    public:
    virtual ~ClosedShape(){}
};

class Line:public OpenShape
{
    public:
    void Draw(){cout << "Draw Line" << endl;}
    void Rotate(){cout << "Rotate Line" << endl;}
    virtual ~Line(){}
};

class Circle:public ClosedShape
{
    public:
    void Draw(){cout << "Draw Circle" << endl;}
    void Rotate(){cout << "Rotate Circle" << endl;}
    virtual ~Circle(){}
};

class Square:public ClosedShape
{
    public:
    void Draw(){cout << "Draw Square" << endl;}
    void Rotate(){cout << "Rotate Square" << endl;}
    virtual ~Square(){}
};


int main()
{
    //Shape S;   //Will not work
    //Shape *S = new Shape(); //Will not work
   
   //Will work
   /* 
    Shape *sptr = new Circle();
    sptr->Rotate();
    sptr->Draw();
    
    delete sptr;
    */
    
    Shape *S1 = new Circle();
    Shape *S2 = new Square();
    Shape *S3 = new Line();
    
    //Try using vector also
    
    delete S1;
    delete S2;
    delete S3;
}

