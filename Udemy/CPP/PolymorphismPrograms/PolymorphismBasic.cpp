#include <iostream>

using namespace std;

class Base
{
    public:
    void SayHello()
    {
        cout << "Hello from base" << endl;
    }
    
};

class Derived:public Base
{
    public:
    void SayHello()
    {
        cout << "Hello from Derived" << endl;
    }
    
};

void Greetings(Base &bObj)
{
    cout << "Greetings" << endl;
    bObj.SayHello();
}

int main()
{
    Base b;
    b.SayHello();
    Derived d;
    d.SayHello();
    
    Greetings(b);
    Greetings(d);

    Base *bptr = new Derived;
    bptr->SayHello();
    
    
    return 0;
}
