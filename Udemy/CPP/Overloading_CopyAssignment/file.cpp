#include <iostream>
#include "file.h"
#include <cstring>

using namespace std;

//no constructor
MyString::MyString():str{nullptr}
{
    str = new char(1);
    *str = '\0';
}

//Single arg constructor or Overload constructor
MyString::MyString(const char *s):str{nullptr}
{
    if(s == nullptr)
    {
        str = new char(1);
        *str = '\0';
    }
    else
    {
        str = new char(strlen(s)+1);
        strcpy(str, s);
    }
        
}

//Copy constructor
MyString::MyString(const MyString &Source):str(nullptr)
{
    str = new char(strlen(Source.str)+1);
    strcpy(str, Source.str);
    
}

//Assignment
MyString &MyString::operator=(const MyString &rhs)
{
    if (this == &rhs)
    {
        return *this;
    }
    delete [] this->str;
    str = new char(strlen(rhs.str));
    strcpy(this->str, rhs.str);
    return *this;
}

//Destructor
MyString::~MyString()
{
    cout << "Destructor" << endl;
    delete []str;
}

//Display
void MyString::Display()
{
    cout << GetLength() << endl;
}

//String getter
char *MyString::GetString()
{
    return str;
}

//Length getter
int MyString::GetLength()
{
    return strlen(str);
}
