#ifndef _FILE_H_
#define _FILE_H_

class MyString 
{
    
    char *str;
    
    public:
    //No arg constructor
    MyString();
    //Single arg constructor
    MyString(const char *s);
    
    //Copy Constructor
    MyString(const MyString &Source);
    
    //copy assignment
    MyString &operator=(const MyString &rhs);
    
    void Display();
    int GetLength();
    char *GetString();
    
    ~MyString();
    
};

#endif