#ifndef _PLAYERS_H_
#define _PLAYERS_H_

class Players
{
    std::string name;
    int health;
    int xp;
    
    static int num_of_players;
    
    public:
    //Constructor
    Players(std::string name_val, int health_val, int xp_val);
    
    static int GetNumOfPlayers();
    
    //Destructor
    ~Players();

    
};

void DisplayActivePlayers();
#endif