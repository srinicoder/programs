#include <iostream>
#include "file.h"

using namespace std;


int Players::num_of_players = 0;

void DisplayActivePlayers()
{
    cout << Players::GetNumOfPlayers() << endl;
}

int Players::GetNumOfPlayers()
{
    return num_of_players;
}

Players::~Players()
{
    num_of_players--;
}

Players::Players(string name_val, int health_val, int xp_val):name{name_val}, health{health_val}, xp{xp_val}
{
        num_of_players++;
        cout << "Constructor" << endl;
        
}