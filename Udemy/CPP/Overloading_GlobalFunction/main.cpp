#include <iostream>
#include "file.h"

using namespace std;

int main()
{
    MyString Srini("Srini");
    MyString Bala("Bala");
    MyString Villian = Srini;
    
    Srini.Display();         //Srini
    Bala.Display();         //Bala
    
    cout << "----------------------------" << endl;
    cout << "Equality" << endl;
    cout << (Srini == Bala) << endl; //Not equal
    cout << (Srini == Villian) << endl; //Equal
    cout << "----------------------------" << endl;
    
    cout << "LowerCase" << endl;
    Srini.Display();
    MyString Srini2 =-Srini;
    Srini2.Display();
    cout << "----------------------------" << endl;
    
    cout << "Concatenate" << endl;
    MyString Hero = Srini+"Ayyadurai";
    Hero.Display();
    
    MyString Two_Hero = Srini + " " + Bala;
    Two_Hero.Display();
    
    MyString Three_Hero = Srini + " " + Bala + "Great";
    Three_Hero.Display();
    cout << "----------------------------" << endl;
    
    return 0;
}
