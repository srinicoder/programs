#include <iostream>
#include "file.h"
#include <cstring>

using namespace std;

//no constructor
MyString::MyString():str{nullptr}
{
    str = new char(1);
    *str = '\0';
}

//Single arg constructor or Overload constructor
MyString::MyString(const char *s):str{nullptr}
{
    if(s == nullptr)
    {
        str = new char(1);
        *str = '\0';
    }
    else
    {
        str = new char(strlen(s)+1);
        strcpy(str, s);
    }
        
}

//Copy constructor
MyString::MyString(const MyString &Source):str(nullptr)
{
    str = new char(strlen(Source.str)+1);
    strcpy(str, Source.str);
    
}

//Assignment
MyString &MyString::operator=(const MyString &rhs)
{
    if (this == &rhs)
    {
        return *this;
    }
    delete [] this->str;
    str = new char(strlen(rhs.str));
    strcpy(this->str, rhs.str);
    return *this;
}

//Equality
bool operator==(const MyString &lhs, const MyString &rhs) 
{
    return (strcmp(lhs.str, rhs.str) == 0);
}

//Make lower case
MyString operator-(const MyString &Obj)
{
    char *buff = new char(strlen(Obj.str)+1);
    strcpy(buff, Obj.str);
    int i;
    for(i = 0; i < strlen(Obj.str);i++)
    {
        buff[i] = tolower(buff[i]);
    }
    
    MyString temp(buff);
    delete [] buff;
    return temp;
}


//Concatenate
MyString operator+(const MyString &lhs, const MyString &rhs)
{
    char *buff = new char(strlen(lhs.str)+1);
    strcpy(buff, lhs.str);
    strcat(buff, rhs.str);
    
    MyString temp(buff);
    delete[] buff;
    return temp;
}



//Destructor
MyString::~MyString()
{
   // cout << "Destructor" << endl;
    delete []str;
}

//Display
void MyString::Display()
{
    cout << str << ": "<< GetLength() << endl;
}

//String getter
char *MyString::GetString()
{
    return str;
}

//Length getter
int MyString::GetLength()
{
    return strlen(str);
}
