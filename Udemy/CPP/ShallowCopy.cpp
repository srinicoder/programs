#include <iostream>

using namespace std;

class Shallow
{
    int *data;
    
    public:
    void SetData(int d);
    int GetData(void);
    
    //Constructor
    Shallow(int d);
    
    //Copy Constructor
    Shallow(const Shallow &S);
    
    
    //Destructor
    ~Shallow(void);
    
};

void Shallow::SetData(int d)
{

}

int Shallow::GetData(void)
{
    return *data;
}

//Constructor
Shallow::Shallow(int d)
{
    data = new int;
    *data = d;
}

//destructor
Shallow::~Shallow(void)
{
    delete data;
}

//Copy Constructor
Shallow::Shallow(const Shallow &S):data{S.data}
{
    cout << "Copy Constructor" << endl;
}

void DisplayShallow(Shallow SOut)
{
    cout << SOut.GetData()<<endl;
}


int main()
{
    Shallow Obj1(100);
    DisplayShallow(Obj1);
    
    Shallow Obj2(Obj1);
    
    Obj2.SetData(1000);
    DisplayShallow(Obj2);
    
    return 0;
}
