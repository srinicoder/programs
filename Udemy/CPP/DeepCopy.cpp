#include <iostream>

using namespace std;

class Deep
{
    int *data;
    
    public:
    void SetData(int d);
    int GetData(void);
    
    //Constructor
    Deep(int d);
    
    //Copy Constructor
    Deep(const Deep &S);
    
    
    //Destructor
    ~Deep(void);
    
};

void Deep::SetData(int d)
{
    *data = d;
}

int Deep::GetData(void)
{
    return *data;
}

//Constructor
Deep::Deep(int d)
{
    data = new int;
    *data = d;
}

//destructor
Deep::~Deep(void)
{
    delete data;
}

//Deep Copy Constructor
Deep::Deep(const Deep &S):Deep{*S.data} //Only change for deep copy
{
    cout << "Copy Constructor" << endl;
}

void DisplayDeep(Deep SOut)
{
    cout << SOut.GetData()<<endl;
}


int main()
{
    Deep Obj1(100);
    DisplayDeep(Obj1);
    
    Deep Obj2(Obj1);
    
    Obj2.SetData(1000);
    DisplayDeep(Obj2);
    
    return 0;
}
