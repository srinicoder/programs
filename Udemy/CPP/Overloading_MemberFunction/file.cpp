#include <iostream>
#include "file.h"
#include <cstring>

using namespace std;

//no constructor
MyString::MyString():str{nullptr}
{
    str = new char(1);
    *str = '\0';
}

//Single arg constructor or Overload constructor
MyString::MyString(const char *s):str{nullptr}
{
    if(s == nullptr)
    {
        str = new char(1);
        *str = '\0';
    }
    else
    {
        str = new char(strlen(s)+1);
        strcpy(str, s);
    }
        
}

//Copy constructor
MyString::MyString(const MyString &Source):str(nullptr)
{
    str = new char(strlen(Source.str)+1);
    strcpy(str, Source.str);
    
}

//Assignment
MyString &MyString::operator=(const MyString &rhs)
{
    if (this == &rhs)
    {
        return *this;
    }
    delete [] this->str;
    str = new char(strlen(rhs.str));
    strcpy(this->str, rhs.str);
    return *this;
}

//Equality
bool MyString::operator==(const MyString &rhs) const 
{
    return (strcmp(str, rhs.str) == 0);
}

//Make lower case
MyString MyString::operator-() const
{
    char *buff = new char(strlen(str)+1);
    strcpy(buff, str);
    int i;
    for(i = 0; i <strlen(str);i++)
    {
        buff[i] = tolower(buff[i]);
    }
    
    MyString temp(buff);
    delete [] buff;
    return temp;
}


//Concatenate
MyString MyString::operator+(const MyString &rhs) const
{
    char *buff = new char(strlen(str)+1);
    strcpy(buff, str);
    strcat(buff, rhs.str);
    
    MyString temp(buff);
    delete[] buff;
    return temp;
}



//Destructor
MyString::~MyString()
{
   // cout << "Destructor" << endl;
    delete []str;
}

//Display
void MyString::Display()
{
    cout << str << ": "<< GetLength() << endl;
}

//String getter
char *MyString::GetString()
{
    return str;
}

//Length getter
int MyString::GetLength()
{
    return strlen(str);
}
