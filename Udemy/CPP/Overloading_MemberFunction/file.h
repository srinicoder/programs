#ifndef _FILE_H_
#define _FILE_H_

class MyString 
{
    
    char *str;
    
    public:
    //No arg constructor
    MyString();
    //Single arg constructor
    MyString(const char *s);
    
    //Copy Constructor
    MyString(const MyString &Source);
    
    //copy assignment
    MyString &operator=(const MyString &rhs);
    
    
    //Overloading
    MyString operator-() const;   //make lowercase
    MyString operator+(const MyString &rhs) const; //concatenate
    bool operator==(const MyString &rhs) const; // Equality
    
    void Display();
    int GetLength();
    char *GetString();
    
    ~MyString();
    
};

#endif