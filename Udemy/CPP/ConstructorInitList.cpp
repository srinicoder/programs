#include <iostream>

using namespace std;

class Players
{
    private:
    string name;
    int health;
    int xp;
    
    public:
    void SetName(string name_val)
    {
        name = name_val;
    }
    
    Players():name{"None"}, health{0}, xp{0}
    {
        cout << "Explicit default constructor" << endl;
    }
    
    Players(string name_val,int health_val):name{name_val}, health{health_val}, xp{0}
    {
        cout << "Parameterized constructor" << endl;
    }
    
    Players(string name_val,int health_val, int xp_val):name{name_val}, health{health_val}, xp{xp_val}
    {
        cout << "Parameterized constructor" << endl;
    }
    
    ~Players()
    {
        cout << "Destructor called for " << name << endl;
    }
    
    
};

int main()
{

    Players splayers;
    splayers.SetName("Srinivasan A");
        
    Players Hero("Hero", 30);
    Hero.SetName("Hero");
    
    Players Villian("Villian", 20, 5);
    Villian.SetName("Villian");
    
    return 0;
}