include <iostream>

using namespace std;

class Players
{
    private:
    string name;
    int health;
    int xp;
    
    public:
    void SetName(string name_val)
    {
        name = name_val;
    }
    
    Players(string name_val = "None",int health_val = 0, int xp_val=0);
    ~Players()
    {
        cout << "Destructor called for " << name << endl;
    }
    
    
};

Players::Players(string name_val,int health_val, int xp_val)
    :name{name_val}, health{health_val}, xp{xp_val}
    {
        cout << "Parameterized constructor" << endl;
    }

int main()
{

    Players splayers;
    splayers.SetName("Srinivasan A");
        
    Players Hero("Hero", 30);
    Hero.SetName("Hero");
    
    Players Villian("Villian", 20, 5);
    Villian.SetName("Villian");
    
    return 0;
}