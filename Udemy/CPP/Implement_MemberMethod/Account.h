#ifndef _ACCOUNT_H_
#define _ACCOUNT_H_
#include <string>

class Account
{
    std::string name;
    int balance;
    
    public:
    std::string GetName(void);
    void SetName(std::string n);
    
    void SetBalance(int amt)
    {
        balance=amt;
    }
    
    int GetBalance(void)
    {
        return balance;
    }
    
    bool Deposit(int n);
    bool Withdrawal(int n);
    
};

#endif