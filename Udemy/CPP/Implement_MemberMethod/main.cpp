#include <iostream>
#include <string>
#include "Account.h"


using namespace std;

int main()
{
    Account Srini;
    Srini.SetName("Srinivasan A");
    Srini.SetBalance(100);
    
    if(Srini.Deposit(500))
    {
        cout << "Deposit Ok.." << endl;
    }
    else
    {
        cout << "Deposit Not Ok.." << endl;
    }
    
    if(Srini.Withdrawal(700))
    {
        cout << "Withdrawal Ok.." << endl;
    }
    else
    {
        cout << "Withdrawal Not Ok.." << endl;
    }
    return 0;
}
