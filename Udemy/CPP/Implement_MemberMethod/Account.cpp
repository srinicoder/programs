#include "Account.h"

//Get name
std::string Account::GetName(void)
{
    return name;
}

//Set name
void Account::SetName(std::string n)
{
    name = n;
}

//Deposit
bool Account::Deposit(int amt)
{
    balance += amt;
    return true;
}

//Withdrawal
bool Account::Withdrawal(int amt)
{
    if((balance-amt)>0)
    {
        balance -= amt;
        return true;
    }
    else
    {
        return false;
    }
}