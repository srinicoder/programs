#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct Node 
{
    int data;
    struct Node *Next;
};
struct Node *first=NULL;

void CreateLL(int a[], int n)
{
    int i;
    struct Node *temp,*last,*p;
    first = (struct Node*)malloc(sizeof(struct Node));
    first->data = a[0];
    first->Next = NULL;
    last = first;
    
    for(i = 1; i < n;i++)
    {
        temp = (struct Node*)malloc(sizeof(struct Node));
        temp->data = a[i];
        temp->Next = NULL;
        last->Next = temp;
        last = temp;
    }
    
}

int CountLL(struct Node *p)
{
    int c = 0;
    while(p != NULL)
    {
        c++;
        p = p->Next;
    }
    return c;
}


void DisplayLL(struct Node *p)
{
    int i;
    while(p!=NULL)
    {
        printf("%d\t", p->data);
        p = p->Next;
    }
    printf("\n");
}

void Insert(struct Node *p, int element, int pos)
{
    int i;
    if(pos < 0 || pos > CountLL(p))
    {
        printf("Position to insert is not in range\n");
        return;
    }
    
    //Insert before first element
    if(pos == 0)
    {
        struct Node* temp = (struct Node*)malloc(sizeof(struct Node));
        temp->data = element;
        temp->Next = first;
        first = temp;
        return;
    }
    
    //Insert Element at given positon
    else if(pos >0)
    {
        for(i = 0;i < pos-1;i++)
        {
            p = p->Next;
        }
        struct Node* temp = (struct Node*)malloc(sizeof(struct Node));
        temp->data = element;
        temp->Next = p->Next;
        p->Next = temp;
        return;
    }
    
}


int main()
{
    int a[] = {2, 4, 6, 8, 9};
    printf("Creating New Link List\n");
    CreateLL(a , 5);
    DisplayLL(first);
    
    printf("Insert 7 before List\n");
    Insert(first, 7, 0);
    DisplayLL(first);
    
    printf("Insert 5 after 2\n");
    Insert(first, 5, 2);
    DisplayLL(first);
    
    printf("Insert 11 after 7\n");
    Insert(first, 11, 7);
    DisplayLL(first);
    return 0;
}
