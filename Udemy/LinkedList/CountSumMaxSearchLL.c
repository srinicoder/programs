#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct Node 
{
    int data;
    struct Node *Next;
};
struct Node *first=NULL;

void CreateLL(int a[], int n)
{
    int i;
    struct Node *temp,*last,*p;
    first = (struct Node*)malloc(sizeof(struct Node));
    first->data = a[0];
    first->Next = NULL;
    last = first;
    
    for(i = 1; i < n;i++)
    {
        temp = (struct Node*)malloc(sizeof(struct Node));
        temp->data = a[i];
        temp->Next = NULL;
        last->Next = temp;
        last = temp;
    }
    
}

void DisplayLL(struct Node *p)
{
    int i;
    while(p!=NULL)
    {
        printf("%d\t", p->data);
        p = p->Next;
    }
    printf("\n");
}

int CountLL(struct Node *p)
{
    int c = 0;
    while(p != NULL)
    {
        c++;
        p = p->Next;
    }
    return c;
}

int SumLL(struct Node *p)
{
    int sum = 0;
    while(p != NULL)
    {
        sum = sum + p->data;
        p = p->Next;
    }
    return sum;
}

int MaxElementLL(struct Node *p)
{
    int max = INT_MIN;
    while(p != NULL)
    {
        if(p->data > max)
        {
            max = p->data;
        }
        p=p->Next;
    }
    return max;
}

struct Node *SearchLL(struct Node *p, int key)
{
    while(p!=NULL)
    {
        if(p->data == key)
        {
            return p;
        }
        p = p->Next;
    }
    return NULL;
}

int main()
{
    int a[] = {2, 4, 6, 8, 9};
    CreateLL(a , 5);
    DisplayLL(first);
    printf("No of Element is %d\n", CountLL(first));
    printf("Sum of Element is %d\n", SumLL(first));
    printf("Max Element is %d\n", MaxElementLL(first));
    printf("Search for Key is %p\n", SearchLL(first, 6));
    
    return 0;
}
