#include <stdio.h>
#include <stdlib.h>

struct Node 
{
  int data;
  struct Node *Next;
};
struct Node *Head = NULL;

int CountCLL(struct Node *p)
{
    int c = 0;
    if(Head!=NULL)
    {
        do
        {
            c++;
            p = p->Next;
        } while(p != Head);
    }
    return c;
}


void CreateCLL(int a[], int n)
{
    int i;
    struct Node *t, *last;
    Head = (struct Node*)malloc(sizeof(struct Node));
    Head->data = a[0];
    Head->Next = Head;
    last = Head;
    
    for(i=1;i<n;i++)
    {
        t = (struct Node*)malloc(sizeof(struct Node));
        t->data = a[i];
        t->Next = last->Next;
        last->Next = t;
        last = t;
    }
}

void InsertCLL(int element, int pos)
{
    int i;
    struct Node *p;
    struct Node *t;
    printf("Count ==>%d\n", CountCLL(Head));
    
    if(pos <0 || pos > CountCLL(Head))
    {
        printf("Position not in range\n");
        return;
    }
    if(pos == 0)
    {
        t = (struct Node *)malloc(sizeof(struct Node));
        if(Head == NULL)
        {
            t->data = element;
            t->Next = t;
            Head=t;
        }
        else
        {
            p=Head;
            while(p->Next!=Head)
            {
                p=p->Next;
            }
            p->Next = t;
            t->data = element;
            t->Next = Head;
            Head = t;
        }
    }
    else
    {
        p=Head;
        for(i=0;i<pos-1;i++)
        {
            p=p->Next;   
        }
        t = (struct Node *)malloc(sizeof(struct Node));
        t->data = element;
        t->Next = p->Next;
        p->Next = t;
    }
}

int DeleteCLL(int pos)
{
    int i, x;
    struct Node *p, *t;
    if(pos==1)
    {
        
        if(p==Head)
        {
            p=Head;
            while(p->Next!=Head)
            {
                p=p->Next;
            }
            x=Head->data;
            free(Head);
            Head=NULL;
            return x;
        }
        else
        {
            p=Head;
            while(p->Next!=Head)
            {
                p=p->Next;
            }
            x=Head->data;
            p->Next=Head->Next;
            free(Head);
            Head=p->Next;
            return x;
        }
    }
    
    else
    {
        p = Head;
        for(i=0;i<pos-2;i++)
        {
            p=p->Next;
        }
        t=p->Next;
        x=t->data;
        p->Next = t->Next;
        free(t);
        return x;
    }
}

void DisplayCLL(struct Node *p)
{
    if(p!=NULL)
    {
        do
        {
            printf("%d\t", p->data);
            p=p->Next;
        }while(p!=Head);
        printf("\n");
    }
}

int main()
{
    /*
    int a[] = {4, 6, 7, 9, 1};
    CreateCLL(a, 5);
    DisplayCLL(Head);
    
    printf("Insert 5 after 3rd position\n");
    InsertCLL(5, 3);
    DisplayCLL(Head);
    */
    
    printf("Insert 11 if Head is NULL or insert before\n");
    InsertCLL(11, 0);
    DisplayCLL(Head);
    
    printf("Insert 13 before Head is NULL or Insert before\n");
    InsertCLL(13, 0);
    DisplayCLL(Head);
    
    printf("Insert 17 after 1st position\n");
    InsertCLL(17, 1);
    DisplayCLL(Head);
    
    printf("Insert 23 after 3rd position\n");
    InsertCLL(23, 3);
    DisplayCLL(Head);
    
    //DeleteCLL
    printf("Deleted node is %d\n" ,DeleteCLL(2));
    DisplayCLL(Head);
    
    printf("Deleted node is %d\n", DeleteCLL(1));
    DisplayCLL(Head);
    
    return 0;
}
