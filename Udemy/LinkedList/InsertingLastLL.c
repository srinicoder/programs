#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct Node 
{
    int data;
    struct Node *Next;
};
struct Node *first= NULL;
struct Node *last = NULL;



void DisplayLL(struct Node *p)
{
    int i;
    while(p!=NULL)
    {
        printf("%d\t", p->data);
        p = p->Next;
    }
    printf("\n");
}


//Always Insert LL at Last
void InsertLastLL(int element)
{
    struct Node *temp = (struct Node*)malloc(sizeof(struct Node));
    temp->data = element;
    temp->Next = NULL;
    
    if(first == NULL)
    {
        first = temp; 
        last = temp;
    }
    else
    {
        last->Next = temp;
        last = temp;
    }
    return;
}


int main()
{
    
    printf("Insert 15 at end\n");
    InsertLastLL(15);
    DisplayLL(first);
    
    printf("Insert 6 at end\n");
    InsertLastLL(6);
    DisplayLL(first);
    
    printf("Insert 9 at end\n");
    InsertLastLL(9);
    DisplayLL(first);
    return 0;
}
