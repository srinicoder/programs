#include <stdio.h>
#include <stdlib.h>


struct Node 
{
    int data;
    struct Node *Next;
};
struct Node *first;

void Create(int a[], int n)
{
    int i;
    struct Node *temp,*last;
    first = (struct Node*)malloc(sizeof(struct Node));
    first->data = a[0];
    first->Next = NULL;
    last = first;
    
    for(i = 1; i < n;i++)
    {
        temp = (struct Node*)malloc(sizeof(struct Node));
        temp->data = a[i];
        temp->Next = NULL;
        last->Next = temp;
        last = temp;
    }
    
}

void Display(void)
{
    int i;
    while(first!=NULL)
    {
        printf("%d\t", first->data);
        first = first->Next;
    }
}


int main()
{
    int a[] = {2, 4, 6, 8, 9};
    Create(a , 5);
    Display();
    
    return 0;
}
