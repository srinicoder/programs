#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct Node 
{
    int data;
    struct Node *Next;
};
struct Node *first= NULL;
struct Node *last = NULL;



void DisplayLL(struct Node *p)
{
    int i;
    while(p!=NULL)
    {
        printf("%d\t", p->data);
        p = p->Next;
    }
    printf("\n");
}

int CountLL(struct Node *p)
{
    int c = 0;
    while(p != NULL)
    {
        c++;
        p = p->Next;
    }
    return c;
}

//Always Insert LL at Last
void InsertLastLL(int element)
{
    struct Node *temp = (struct Node*)malloc(sizeof(struct Node));
    temp->data = element;
    temp->Next = NULL;
    
    if(first == NULL)
    {
        first = temp; 
        last = temp;
    }
    else
    {
        last->Next = temp;
        last = temp;
    }
    return;
}

int DeleteLL(int pos)
{
    int i, x;
    struct Node *p;
    struct Node *temp;
    
    if(pos<0 || pos > CountLL(first))
    {
        printf("Position out of range\n");
        return;
    }
    
    if(pos == 0)
    {
        temp = first;
        x = temp->data;
        first = first->Next;
        temp->Next=NULL;
        free(temp);
        return x;
    }
    else
    {
        p = first;
        for(i=0;i<pos-1 && p!=NULL;i++)
        {
            p = p->Next;
        }
        temp = p->Next;
        x = temp->data;
        p->Next = p->Next->Next;
        free(temp);
        return x;
    }
}

int main()
{
    
    printf("Insert 15 at end\n");
    InsertLastLL(15);
    DisplayLL(first);
    
    printf("Insert 6 at end\n");
    InsertLastLL(6);
    DisplayLL(first);
    
    printf("Insert 9 at end\n");
    InsertLastLL(9);
    DisplayLL(first);
    
    printf("Insert 8 at end\n");
    InsertLastLL(8);
    DisplayLL(first);
    
    printf("Insert 13 at end\n");
    InsertLastLL(13);
    DisplayLL(first);
    
    //Delete linked List
    printf("Deleted first node with element %d\n", DeleteLL(0));
    DisplayLL(first);
    
    printf("Deleted Second node with element %d\n", DeleteLL(1));
    DisplayLL(first);
    
    printf("Deleted third node with element %d\n", DeleteLL(2));
    DisplayLL(first);
    return 0;
}
