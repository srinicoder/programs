#include <stdio.h>
#include <stdlib.h>

struct Node
{
    struct Node *Prev;
    int data;
    struct Node *Next;
};
struct Node *First=NULL;

int CountDLL(struct Node *p);

int CountDLL(struct Node *p)
{
    int c = 0;
    while(p!=NULL)
    {
        c++;
        p=p->Next;
    }
    return c;
}

void CreateDLL(int A[], int n)
{
    int i;
    struct Node *p, *last, *t;
    First = (struct Node*)malloc(sizeof(struct Node));
    First->data = A[0];
    First->Prev = NULL;
    First->Next = NULL;
    last=First;
    
    for(i=1;i<n;i++)
    {
        t = (struct Node*)malloc(sizeof(struct Node));
        t->data = A[i];
        t->Prev = last;    
        t->Next = NULL;
        last->Next = t;
        last=last->Next;
    }
}

void InsertDLL(int element, int pos)
{
    int i;
    struct Node *p, *q, *t;
    int len = CountDLL(First);
    if(pos < 0 || pos > len)
    {
        printf("Position not in range\n");
        return;
    }
    if(pos==0)
    {
        t = (struct Node*)malloc(sizeof(struct Node));
        t->Prev = NULL;
        t->data = element;
        t->Next = First;
        First->Prev = t;
        First = t;
    }
    else
    {
        p = First;
        for(i = 0;i<pos-1;i++)
        {
            p=p->Next;
        }
        t = (struct Node*)malloc(sizeof(struct Node));
        q = p->Next;
        t->data = element;
        t->Next = p->Next;
        t->Prev = p;
        if(q!=NULL)
        {
            q->Prev = t;
        }
        p->Next = t;
    }
    
}

int DeleteDLL(int pos)
{
    int x, i;
    struct Node *p,*q,*t;
    if(pos < 0 || pos > CountDLL(First))
    {
        printf("Position is Out of Range\n");
        return -1;
    }
    if(pos == 1)
    {
        p = First;
        x = p->data;
        First = p->Next;
        free(p);
        if(First!=NULL)
        {
            First->Prev = NULL;
        }
        return x; 
    }
    else
    {
        p = First;
        for(i = 0;i < pos-2;i++)
        {
            p=p->Next;
        }
        t = p->Next;
        q = t->Next;
        x = t->data;
        p->Next = q;
        q->Prev = p;
        free(t);
        return x;
    }
}

void DisplayDLL(struct Node *p)
{
    while(p!=NULL)
    {
        printf("%d\t", p->data);
        p=p->Next;
    }
    printf("\n");
}

int main()
{
    int ret;
    int a[] = {5, 4, 7, 1, 8};
    CreateDLL(a, 5);
    printf("Create DLL\n");
    DisplayDLL(First);

    InsertDLL(13, 0);
    DisplayDLL(First);
    
    InsertDLL(12, 6);
    DisplayDLL(First);
    
    ret = DeleteDLL(1);
    if(ret ==-1)
    {
        printf("Position out of range\n");
    }
    else
    {
        printf("Deleted %d in position 1 from List\n", ret);
    }
    DisplayDLL(First);

    ret = DeleteDLL(2);
    if(ret ==-1)
    {
        printf("Positon out of range\n");
    }
    else
    {
        printf("Deleted %d in position 2 from List\n", ret);
        
    }
    DisplayDLL(First);
    
    ret = DeleteDLL(7);
    if(ret ==-1)
    {
        printf("Positon out of range\n");
    }
    else
    {
        printf("Deleted %d in position 7 from List\n", ret);
        
    }
    DisplayDLL(First);
    
    return 0;
}
